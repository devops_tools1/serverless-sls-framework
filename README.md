# Serverless Framework

## Sobre el proyecto

> A continuación se prensentan ejemplos de Severless

## Antes de empezar

<p style='text-align: justify;'> Para los siguientes ejemplos estan pensados para ser probados utilizando un contenedor de Docker, por lo que se provee el dockerfile necesario para poder instalar dependencias y todo lo que se necesita para poder correr y probar cada uno de los ejemplo. Si quieres mayor informacion de como instalar docker en tu computadora local puedes dirigirte al siguiente enlace: </p>

* [Instalacion de Docker](https://docs.docker.com/desktop/)

* [Creadenciales de AWS](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)

* [Serverless Framework](https://www.serverless.com/framework/docs/providers/aws)

### Como correr los ejemplos

<p style='text-align: justify;'> 
Para poder correr los ejemplos, este repositorio cuenta con un Dockerfile que permite crear el contenedor con el ambiente ya preparado para poder probar los ejemplos. Para ejecutarlos puedes seguir los siguientes pasos
</p>

1. Construye el contenedor de Docker

```bash
$ docker build -t <NOMBRE_CONTENEDOR>:<TAG_CONTENEDOR> .
```

2. Corre el contenedor y monta el directorio de **terraform** en el contenedor

```bash
$ docker run -it --rm -v ~/.aws:/root/.aws -v $PWD/home <NOMBRE_CONTENEDOR>:<TAG_CONTENEDOR>
```

**NOTA**: Es importante saber como crear credenciales para la cuenta de AWS, ya que se esta montando el directorio **~/.aws** de la maquina local a el contendor, esto para poder utilizar credenciales dentro del contendor al desplegar utilizando terraform.

3. Ahora te encontraras en el contenedor y ya puedes probar los ejemplos:

```bash
➜  ~ sls --help
```

## Documentación de referencia:

* [Lenguaje de Terraform](https://www.terraform.io/docs/language/index.html)
* [AWS provider para Terraform](https://registry.terraform.io/providers/hashicorp/aws/latest)

### Youtube Playlist

* [Playlist de video ejemplos](https://youtube.com/playlist?list=PL8Xnw9lMxPCGYpntz2JIZKVms1M5I7fYi)
