FROM node:16

RUN apt update

RUN apt install -y wget curl unzip git

RUN mkdir /root/.aws

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"

RUN unzip awscliv2.zip

RUN ./aws/install

RUN curl -o- -L https://slss.io/install | bash

WORKDIR /home