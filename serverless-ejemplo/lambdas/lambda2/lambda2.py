import json
import os

def handler(event, context):
    
    body = {
        "message": "Lambda 2",
        "lamdba": os.environ['LambdaName'],
        "environment": os.environ['Env']
    }

    

    response = {"statusCode": 200, "body": json.dumps(body)}

    return response